# Void World

Remove all blocks from the default generator

## Usage

Install the plugin onto your bukkit, spigot or paper server.

For the worlds that you wish to use the void generator add the following to your `bukkit.yml`, replacing "world" with the name of the world:

```yaml
worlds:
    world:
        generator: VoidWorld
```
