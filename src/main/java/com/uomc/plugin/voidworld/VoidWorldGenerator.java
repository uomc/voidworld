package com.uomc.plugin.voidworld;

import org.bukkit.generator.ChunkGenerator;

public class VoidWorldGenerator extends ChunkGenerator {
    public boolean shouldGenerateCaves() {
        return false;
    }
    public boolean shouldGenerateDecorations() {
        return false;
    }
    public boolean shouldGenerateMobs() {
        return false;
    }
    public boolean shouldGenerateNoise() {
        return false;
    }
    public boolean shouldGenerateStructures() {
        return false;
    }
    public boolean shouldGenerateSurface() {
        return false;
    }
}
